<?php include 'components/header.php'; ?>
<div class="login-page">
    <div class="login-blk">
        <img src="./assets/img/logo-white.svg" class="logo">
        <form class="form-block">
            <h1>LOGIN</h1>
            <div class="form-field">
                <label>Username</label>
                <input type="text" placeholder="Type your username">
            </div>
            <div class="form-field form-field-password">
                <label>Password</label>
                <input type="password" placeholder="Type your password">
                <a href="javascript:void(0)" class="disabled">Show</a>
            </div>
            <div class="form-field checkbox-c">
                <label for="remember-me">Remember me</label>
                <input type="checkbox" placeholder="Type your password" id="remember-me">
            </div>
            <div class="form-field">
                <button type="submit" class="primary-btn disabled-btn">Login</button>
            </div>
        </form>
    </div>
</div>
<?php include 'components/footer.php'; ?>