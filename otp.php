<?php include 'components/header.php'; ?>
    <div class="otp-page">
        <a href="/" class="go-back"><img src="./assets/img/left-arrow-white.svg"> Back to login </a>
        <div class="login-blk">
            <img src="./assets/img/logo-white.svg" class="logo">
            <form class="form-block">
                <h1>ACCOUNT VERIFICATION</h1>
                <p>A 6-digit OTP has been sent to your<br> registered mobile number.</p>
                <div class="form-field">
                    <label>OTP</label>
                    <div class="otp-field">
                        <input type="text" maxlength="1">
                        <input type="text" maxlength="1">
                        <input type="text" maxlength="1">
                        <input type="text" maxlength="1">
                        <input type="text" maxlength="1">
                        <input type="text" maxlength="1">
                    </div>
                    <div class="resend-blk">Didn’t receive an OTP? <a href="#">Resend</a> </div>
                </div>
                <div class="form-field">
                    <button type="submit" class="primary-btn disabled-btn">Verify</button>
                </div>
            </form>
        </div>
    </div>
<?php include 'components/footer.php'; ?>