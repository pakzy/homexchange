<div class="side-bar">
  <ul>
    <li>
      <a href="#">
        <img src="./assets/img/home.svg"> Home
      </a>
    </li>
    <li>
      <div class="accordion">
        <div class="accordion-item">
          <a href="#" class="accordion-button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTow">
            <img src="./assets/img/users.svg"> Users
          </a>
          <div id="collapseTwo" class="accordion-collapse collapse show">
            <ul>
              <li>
                <a href="/hm/user.php" class="active">All users</a>
              </li>
              <li>
                <a href="#">second sub item</a>
              </li>
              <li>
                <a href="#">third one</a>
              </li>
              <li>
                <a href="#">and fourth</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </li>
    <li>
      <a href="#">
        <img src="./assets/img/role.svg"> Roles
      </a>
    </li>
  </ul>
</div>