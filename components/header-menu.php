<header>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <a href="#" class="block d-sm-none menu-burger">
          <img src="./assets/img/menu.svg">
        </a>
        <a href="/hm">
          <img src="./assets/img/main-logo.svg">
        </a>
      </div>
      <div class="col-md-6">
        <ul class="float-end mb-0">
          <li class="notification">
            <a href="#">
              <img src="./assets/img/bell.svg" width="18">
            </a>
          </li>
          <li class="user-account">
            <div class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                <img src="./assets/img/user.png">
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item" href="#">Action</a></li>
                <li><a class="dropdown-item" href="#">Another action</a></li>
                <li><a class="dropdown-item" href="#">Something else here</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</header>