<div class="edit-role-popup popup-modal">
    <div class="add-user-blk">
        <h1>Edit role</h1>
        <a href="javascript:void(0)" class="close-popup">
            <img src="./assets/img/close.svg">
        </a>


        <div class="form-blk">
            <div class="row">
                <div class="col col-md-12">
                    <div class="form-field">
                        <label>Role</label>
                        <input type="text" placeholder="Enter role name" value="Relationship Manager">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col col-md-12">
                    <div class="form-field textarea-field">
                        <label>Description <span>Max 160 characters</span></label>
                        <textarea placeholder="Enter the role description">Engage and hand-hold a client through out the journey until the deal is signed with HX</textarea>
                    </div>
                </div>
            </div>

            <div class="row radio-row">
                <div class="col col-md-12">
                    <div class="form-field">
                        <label>Permissions & Access</label>
                        <div class="row">
                            <div class="col offset-md-4 col-md-2">
                                <h3>View/Edit</h3>
                            </div>
                            <div class="col col-md-2">
                                <h3>View</h3>
                            </div>
                            <div class="col col-md-2">
                                <h3>No access</h3>
                            </div>
                            <div class="col col-md-2">
                                <h3>By field</h3>
                            </div>
                        </div>
                        <div class="accordion permission-accordion">
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <div class="row">
                                        <div class="col col-md-4">
                                            <button class="accordion-button" type="button">
                                                General information
                                            </button>
                                        </div>
                                        <div class="col col-md-2">
                                            <div class="form-field form-field-radio">
                                                <label for="radio-1" class="form-field-radio-active"></label>
                                                <input type="radio" id="radio-1" name="generalInfo" checked>
                                            </div>
                                        </div>
                                        <div class="col col-md-2">
                                            <div class="form-field form-field-radio">
                                                <label for="radio-2"></label>
                                                <input type="radio" id="radio-2" name="generalInfo">
                                            </div>
                                        </div>
                                        <div class="col col-md-2">
                                            <div class="form-field form-field-radio">
                                                <label for="radio-3"></label>
                                                <input type="radio" id="radio-3" name="generalInfo">
                                            </div>
                                        </div>
                                        <div class="col col-md-2">
                                            <div class="form-field form-field-radio by-filed">
                                                <label for="radio-4"></label>
                                                <input type="radio" id="radio-4" name="generalInfo">
                                            </div>
                                        </div>
                                    </div>
                                </h2>
                                <div class="accordion-collapse">
                                    <div class="accordion-body">

                                        <div class="row">
                                            <div class="col col-md-4">
                                                <button class="accordion-button" type="button">
                                                    Customer Enquiry code
                                                </button>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-1-1"></label>
                                                    <input type="radio" id="radio-1-1" name="code">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-1-2"></label>
                                                    <input type="radio" id="radio-1-2" name="code">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-1-3"></label>
                                                    <input type="radio" id="radio-1-3" name="code">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col col-md-4">
                                                <button class="accordion-button" type="button">
                                                    Lead Source
                                                </button>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-2-1"></label>
                                                    <input type="radio" id="radio-2-1" name="code11">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-2-2"></label>
                                                    <input type="radio" id="radio-2-2" name="code11">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-2-3"></label>
                                                    <input type="radio" id="radio-2-3" name="code11">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col col-md-4">
                                                <button class="accordion-button" type="button">
                                                    Lead Sub source
                                                </button>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-3-1"></label>
                                                    <input type="radio" id="radio-3-1" name="code22">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-3-2"></label>
                                                    <input type="radio" id="radio-3-2" name="code22">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-3-3"></label>
                                                    <input type="radio" id="radio-3-3" name="code22">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <div class="row">
                                        <div class="col col-md-4">
                                            <button class="accordion-button" type="button">
                                                OH details
                                            </button>
                                        </div>
                                        <div class="col col-md-2">
                                            <div class="form-field form-field-radio">
                                                <label for="radio-1-22" class="form-field-radio-active"></label>
                                                <input type="radio" id="radio-1-22" name="generalInfo2" checked>
                                            </div>
                                        </div>
                                        <div class="col col-md-2">
                                            <div class="form-field form-field-radio">
                                                <label for="radio-2-22"></label>
                                                <input type="radio" id="radio-2-22" name="generalInfo2">
                                            </div>
                                        </div>
                                        <div class="col col-md-2">
                                            <div class="form-field form-field-radio">
                                                <label for="radio-3-22"></label>
                                                <input type="radio" id="radio-3-22" name="generalInfo2">
                                            </div>
                                        </div>
                                        <div class="col col-md-2">
                                            <div class="form-field form-field-radio by-filed">
                                                <label for="radio-4-22"></label>
                                                <input type="radio" id="radio-4-22" name="generalInfo2">
                                            </div>
                                        </div>
                                    </div>
                                </h2>
                                <div class="accordion-collapse">
                                    <div class="accordion-body">

                                        <div class="row">
                                            <div class="col col-md-4">
                                                <button class="accordion-button" type="button">
                                                    Customer Enquiry code
                                                </button>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-1-1-2"></label>
                                                    <input type="radio" id="radio-1-1-2" name="code2">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-1-2-2"></label>
                                                    <input type="radio" id="radio-1-2-2" name="code2">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-1-3-2"></label>
                                                    <input type="radio" id="radio-1-3-2" name="code2">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col col-md-4">
                                                <button class="accordion-button" type="button">
                                                    Lead Source
                                                </button>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-2-1-2"></label>
                                                    <input type="radio" id="radio-2-1-3" name="code3">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-2-2"></label>
                                                    <input type="radio" id="radio-2-2-3" name="code3">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-2-3"></label>
                                                    <input type="radio" id="radio-2-3-3" name="code3">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col col-md-4">
                                                <button class="accordion-button" type="button">
                                                    Lead Sub source
                                                </button>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-3-1-4"></label>
                                                    <input type="radio" id="radio-3-1-4" name="code4">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-3-2-4"></label>
                                                    <input type="radio" id="radio-3-2-4" name="code4">
                                                </div>
                                            </div>
                                            <div class="col col-md-2">
                                                <div class="form-field form-field-radio">
                                                    <label for="radio-3-3-4"></label>
                                                    <input type="radio" id="radio-3-3-4" name="code4">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="submit-row">
                <div class="form-field">
                    <a href="#" class="secondary-btn close-popup">
                        Cancel
                    </a>
                    <a href="#" class="primary-btn">
                        Save
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>