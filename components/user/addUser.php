<div class="add-user-popup popup-modal">
  <div class="add-user-blk">
    <h1>Add new user</h1>
    <a href="javascript:void(0)" class="close-popup">
      <img src="./assets/img/close.svg">
    </a>
    <div class="add-user-upload">
      <img src="./assets/img/dummy-user.png">
      <label for="file-upload">+ Upload </label>
      <input type="file" id="file-upload">
    </div>

    <div class="form-blk">
      <div class="row">
        <div class="col-md-12">
          <div class="form-field">
            <label>Assign a role</label>
            <div class="custom-select">
              <select>
                <option value="1">Select a role</option>
                <option value="2">Select a role 1</option>
                <option value="3">Select a role 2</option>
              </select>
            </div>

          </div>
        </div>
      </div>

      <div class="row row-2">
        <div class="col col-md-6">
          <div class="form-field">
            <label>First name</label>
            <input type="text" placeholder="Type your first name">
            <span class="wrong-val">
                <img src="./assets/img/wrong-icon.svg"> Error Text
            </span>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-field">
            <label>Last name </label>
            <input type="text" placeholder="Type your last name">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="form-field">
            <label>Email address</label>
            <input type="text" placeholder="Type your email address">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="form-field form-field-phone">
            <label>Phone number</label>
            <div class="custom-select disabled">
              <select>
                <option selected disabled>+91</option>
              </select>
            </div>
            <input type="text" placeholder="Type your mobile number">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="form-field">
            <label>Location</label>
            <input type="text" placeholder="Type your location">
          </div>
        </div>
      </div>

      <div class="submit-row">
        <div class="form-field">
          <a href="#" class="secondary-btn close-popup">
            Cancel
          </a>
          <a href="#" class="primary-btn disabled-btn">
            Save
          </a>
        </div>
      </div>
    </div>

  </div>
</div>