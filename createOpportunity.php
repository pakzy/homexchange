<main class="create-opportunity-page c-o-p">
  <?php include 'components/header.php'; ?>
  <?php include 'components/header-menu.php'; ?>
  <?php include 'components/sidebar.php'; ?>
    <section class="main-section">
        <div class="container-fluid center-container">
            <div class="row">
                <div class="col-md-4">

                    <div class="user-detail">
                        <a href="#">
                            <img src="assets/img/left-arrow-red.svg">
                        </a>
                        <div>
                            <h1>Create new</h1>
                            <div class="user-name active">
                                PS
                                <div class="user-tooltip-blk">
                                    <p>Relationship Manager</p>
                                    <h4>Prakash Singh</h4>
                                    <a href="#">
                                        <img src="assets/img/phone.svg"> 9012345678
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="l-c-block">
                        <div class="gallery-blk">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Old Home Details</button>
                                    <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Old Home Photos</button>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">...</div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2>All Photos(40) <a href="#">View All</a> </h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                          <?php for ($j = 0; $j < 4; $j++){ ?>
                                              <div class="row mt-5">
                                                  <div class="col-md-12">
                                                      <h6>Kitchen(4)</h6>
                                                  </div>
                                                <?php for ($i = 0; $i < 4; $i++){ ?>
                                                    <div class="col-md-4 mb-3 image-blk">
                                                        <img src="assets/img/image1.jpg" data-bs-toggle="modal" data-bs-target="#exampleModal-<?php echo $j; ?>">
                                                    </div>
                                                <?php } ?>
                                              </div>
                                              <!-- Modal -->
                                              <div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" id="exampleModal-<?php echo $j; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog modal-xl">
                                                      <div class="modal-content">
                                                          <div class="modal-header">
                                                              <h2 id="exampleModalLabel">Photos (42)</h2>
                                                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                          </div>
                                                          <div class="modal-body">
                                                              <div id="carousel-cat" class="carousel slide" data-bs-ride="carousel" data-interval="false">
                                                                  <div class="carousel-inner">
                                                                      <?php $z = 0; for ($i = 0; $i < 10; $i++){ ?>
                                                                          <div class="carousel-item <?php if($i == 0) { echo 'active'; } ?>">
                                                                              <ul class="row cat-list">
                                                                                  <?php for ($j = 0; $j < 8; $j++){ ?>
                                                                                      <li class="col">
                                                                                          <a href="#" class="<?php if($z == 0) { echo 'active-item'; } ?>">Hall (4)</a>
                                                                                      </li>
                                                                                  <?php $z = 1; } ?>
                                                                              </ul>
                                                                          </div>
                                                                      <?php } ?>


                                                                      <div class="carousel-item">
                                                                          <ul class="row cat-list">
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                          </ul>
                                                                      </div>

                                                                      <div class="carousel-item">
                                                                          <ul class="row cat-list">
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                          </ul>
                                                                      </div>

                                                                      <div class="carousel-item">
                                                                          <ul class="row cat-list">
                                                                              <li class="col">
                                                                                  <a href="#">Hall (4)</a>
                                                                              </li>
                                                                          </ul>
                                                                      </div>


                                                                  </div>
                                                                  <button class="carousel-control-prev" type="button" data-bs-target="#carousel-cat" data-bs-slide="prev">
                                                                      <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                          <path d="M6.33301 2L2.33301 6L6.33301 10" stroke="#9E1C2C" stroke-width="2" stroke-linecap="square"/>
                                                                      </svg>

                                                                  </button>
                                                                  <button class="carousel-control-next" type="button" data-bs-target="#carousel-cat" data-bs-slide="next">
                                                                      <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                          <path d="M1.66699 10L5.66699 6L1.66699 2" stroke="#9E1C2C" stroke-width="2" stroke-linecap="square"/>
                                                                      </svg>
                                                                  </button>
                                                              </div>

                                                              <div id="carousel-gallery" class="carousel slide mt-5 mb-5" data-bs-ride="carousel" data-interval="false">
                                                                  <div class="carousel-inner">
                                                                      <div class="carousel-item active">
                                                                          <img src="assets/img/image1.jpg" alt="...">
                                                                      </div>
                                                                      <div class="carousel-item">
                                                                          <img src="assets/img/image1.jpg" alt="...">
                                                                      </div>
                                                                      <div class="carousel-item">
                                                                          <img src="assets/img/image1.jpg" alt="...">
                                                                      </div>
                                                                  </div>
                                                                  <button class="carousel-control-prev" type="button" data-bs-target="#carousel-gallery" data-bs-slide="prev">
                                                                      <svg width="19" height="32" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                          <path d="M6.33301 2L2.33301 6L6.33301 10" stroke="#9E1C2C" stroke-width="2" stroke-linecap="square"/>
                                                                      </svg>

                                                                  </button>
                                                                  <button class="carousel-control-next" type="button" data-bs-target="#carousel-gallery" data-bs-slide="next">
                                                                      <svg width="19" height="32" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                          <path d="M1.66699 10L5.66699 6L1.66699 2" stroke="#9E1C2C" stroke-width="2" stroke-linecap="square"/>
                                                                      </svg>
                                                                  </button>
                                                              </div>

                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 f-block-col">
                    <div class="f-block">
                        <div class="f-block-1">
                            <h1>
                                <img src="assets/img/file.svg">
                                <span>Creating new opportunity<br> still need to finish </span>
                            </h1>
                        </div>
                        <div class="f-block-2">
                            <h2>Data Collection</h2>
                            <h3>Lead Details</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-field">
                                        <h6>Full name <span>*</span></h6>
                                        <input type="text" placeholder="Type your last name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-field">
                                        <h6>Mobile No <span>*</span></h6>
                                        <input type="text" placeholder="Type your last name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-field">
                                        <h6>Alternate contact No. <span>*</span></h6>
                                        <input type="text" placeholder="Type your last name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-field emal">
                                        <h6>Email ID <span>*</span></h6>
                                        <input type="text" placeholder="Type your last name">
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4"></div>
                                <div class="col-md-12 add-divider">testing</div>
                                <div class="col-md-6 two-fields">
                                    <div class="form-field">
                                        <h6>Budget</h6>
                                        <input type="text" placeholder="Type your last name">
                                    </div>
                                </div>
                                <div class="col-md-6 two-fields left-label">
                                    <div class="form-field">
                                        <h6>To</h6>
                                        <input type="text" placeholder="Type your last name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-field">
                                        <h6>Lead campaign</h6>
                                        <div class="custom-select">
                                            <select>
                                                <option value="1">Choose an option</option>
                                                <option value="2">Select a role 1</option>
                                                <option value="3">Select a role 2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-field">
                                        <h6>Lead source <span>*</span></h6>
                                        <div class="custom-select">
                                            <select>
                                                <option value="1">Choose an option</option>
                                                <option value="2">Select a role 1</option>
                                                <option value="3">Select a role 2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-field">
                                        <h6>Lead sub-source <span>*</span></h6>
                                        <div class="custom-select">
                                            <select>
                                                <option value="1">Choose an option</option>
                                                <option value="2">Select a role 1</option>
                                                <option value="3">Select a role 2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h6>Sell exisiting property</h6>
                                    <div class="form-field form-field-radio">
                                        <label for="radio-3-1-4"></label>
                                        <input type="radio" id="radio-3-1-4" name="code4"> Yes
                                    </div>
                                    <div class="form-field form-field-radio">
                                        <label for="radio-3-1-5"></label>
                                        <input type="radio" id="radio-3-1-5" name="code4"> No
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h6>Sell exisiting property</h6>
                                    <div class="form-field radiobutton">
                                        <input type="radio" name="test" id="radio-3-1-4-1">
                                        <label for="radio-3-1-4-1">
                                            <span></span>
                                            Bad
                                        </label>
                                    </div>
                                    <div class="form-field radiobutton">
                                        <input type="radio" name="test" id="radio-3-1-4-2">
                                        <label for="radio-3-1-4-2">
                                            <span></span>
                                            property
                                        </label>
                                    </div>
                                    <div class="form-field radiobutton">
                                        <input type="radio" name="test" id="radio-3-1-4-3">
                                        <label for="radio-3-1-4-3">
                                            <span></span>
                                            exisiting property
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h6>Purchase another home  *</h6>
                                    <div class="custom-select">
                                        <select>
                                            <option value="1">Choose an option</option>
                                            <option value="2">Select a role 1</option>
                                            <option value="3">Select a role 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h6>Is a govt body alloted property  *</h6>
                                    <div class="custom-select">
                                        <select>
                                            <option value="1">Choose an option</option>
                                            <option value="2">Select a role 1</option>
                                            <option value="3">Select a role 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row submit-row">
                                <div class="col-md-4">
                                    <h4><img src="assets/img/calender.svg"> Schedule Follow-Up</h4>
                                </div>
                                <div class="col-md-8">
                                    <a href="#" class="secondary-btn">Save</a>
                                    <a href="#" class="primary-btn disabled-btn">Submit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="t-block">
                        <div class="t-block-1">

                        </div>
                        <div class="t-block-2">
                            <div class="t-block-2-1">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="timeline-tab" data-bs-toggle="tab" data-bs-target="#timeline" type="button" role="tab" aria-controls="timeline" aria-selected="false">Timeline</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="notes-tab" data-bs-toggle="tab" data-bs-target="#notes" type="button" role="tab" aria-controls="notes" aria-selected="true">Notes</button>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade" id="timeline" role="tabpanel" aria-labelledby="timeline-tab">...</div>
                                    <div class="tab-pane fade show active" id="notes" role="tabpanel" aria-labelledby="notes-tab">

                                    </div>
                                </div>
                            </div>
                            <div class="row submit-row">
                                <div class="col-md-12">
                                    <label class="attachment-icon">
                                        <img src="assets/img/attachment.svg">
                                        <input type="file">
                                    </label>
                                    <div contenteditable></div>
                                    <a href="#">Cancel</a>
                                    <a href="#">Save</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<!-- Modal -->
<div class="modal fade" id="confirm-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <img src="assets/img/close.svg">
                </button>
                <div class="confirm-blk">
                    <img src="assets/img/visibility.svg">
                    <h2>Are you sure you want to make this note public?</h2>
                    <p>Changes saved cannot be reverted. </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="secondary-btn" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="primary-btn">Yes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="schedule-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Schedule Follow-Up</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <img src="assets/img/close.svg">
                </button>
                <p class="success-msg">Your progress has been auto saved!</p>
            </div>
            <div class="modal-body">
                <div class="m-block">
                    <h6>Select the reason </h6>
                    <div class="form-field form-field-radio">
                        <label for="radio-1"></label>
                        <input type="radio" id="radio-1" name="code"> Customer did not have time
                    </div>
                    <div class="form-field form-field-radio active">
                        <label for="radio-2" class="form-field-radio-active"></label>
                        <input type="radio" id="radio-2" name="code" checked> Customer did not respond/Busy
                    </div>
                    <div class="form-field form-field-radio">
                        <label for="radio-3"></label>
                        <input type="radio" id="radio-3" name="code"> Provided alternate number
                        <div class="form-field">
                            <h6>Alternate contact no. <span>*</span></h6>
                            <input type="text" placeholder="Type the number">
                        </div>
                    </div>
                    <div class="form-field form-field-radio">
                        <label for="radio-3"></label>
                        <input type="radio" id="radio-3" name="code"> Other
                        <div class="form-field textarea-field">
                            <h6>Please provide reason <span>Max 160 characters</span></h6>
                            <textarea placeholder="Enter the role description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="m-block-2 row">
                    <div class="col-md-6">
                        <h6>Calendar</h6>
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" placeholder="dd/mm/yyyy">
                            <div class="input-group-addon">
                                <img src="assets/img/datepicker.svg">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-field time-form-field">
                            <h6>Time</h6>
                            <input type="text" placeholder="00:00">
                            <div>
                                <a href="#">AM</a>
                                <a href="#" class="active">PM</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="secondary-btn" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="primary-btn">Yes</button>
            </div>
        </div>
    </div>
</div>

  <script>
      $(function () {
          $("[data-bs-toggle=tooltip").tooltip({
              delay: { "show": 500, "hide": 1000000000 }
          });
          let dataTables = $('#hm-datatable-opportunities').DataTable({
              columnDefs: [ {
                  targets: 0,
                  searchable: false,
                  orderable: false,
                  className: 'select-checkbox',
                  render: function (data, type, full, meta){
                      return '<span><input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '"></span>';
                  }
              },
                  {
                      orderable: false,
                      targets:   8
                  } ],
              language: {
                  searchPlaceholder: "Search",
                  search: "",
                  lengthMenu: "Rows per page _MENU_",
                  paginate: {
                      previous: "",
                      next: ""
                  }
              },
              select: {
                  style:    'os',
                  selector: 'td:first-child span'
              },
              order: [[ 2, 'asc' ]],
              orderCellsTop: true,
              fixedHeader: true,
              initComplete: function () {
                  var api = this.api();
                  // For each column
                  api
                      .columns()
                      .eq(0)
                      .each(function (colIdx) {
                          $(document).click(function (e) {
                              let $this = $(e.target);
                              if($this.hasClass('same-as-selected')){
                                  let val = $this.text();
                                  let data = $this.closest('.custom-select').attr('data-id');
                                  if(val == 'All'){
                                      val = '';
                                  }
                                  if(data == colIdx){
                                      api
                                          .column(colIdx)
                                          .search(val)
                                          .draw();
                                  }
                              }
                          });
                      });

                  let x = $('.bulk-action').offset();
                  $('.dataTables_filter').css({
                      left: (x.left - 490)
                  });
              },
          });
          dataTables.on("click", "th.select-checkbox span", function() {
              var rows = dataTables.rows({ 'search': 'applied' }).nodes();
              let $this = $(this).closest('th');
              if($this.find('input').is(':checked')){
                  $this.removeClass('selected unselected');
                  $this.find('input').prop('checked', false);
                  $('input[type="checkbox"]', rows).prop('checked', false);
                  $('td.select-checkbox').removeClass('selected');
              } else {
                  $this.addClass('selected').removeClass('unselected');
                  $this.find('input').prop('checked', true);
                  $('input[type="checkbox"]', rows).prop('checked', true);
                  $('td.select-checkbox').addClass('selected');
              }

              // if ($("th.select-checkbox").hasClass("selected")) {
              //     dataTables.rows().deselect();
              //     $("th.select-checkbox").removeClass("selected");
              // } else {
              //     dataTables.rows().select();
              //     $("th.select-checkbox").addClass("selected");
              // }
          })
          dataTables.on("click", "td.select-checkbox span ", function(e) {
              let $this = $(this).closest('td');
              if($this.find('input').is(':checked')){
                  $this.removeClass('selected');
                  $this.find('input').prop('checked', false);
              } else {
                  $this.addClass('selected');
                  $this.find('input').prop('checked', true);
              }
              $('td.select-checkbox').each(function () {
                  if(!$(this).find('input').is(':checked')){
                      $('th.select-checkbox').addClass('unselected');
                  }
              })
              // $(this).toggleClass('selected');
              // if (dataTables.rows({
              //     selected: true
              // }).count() !== dataTables.rows().count()) {
              //     $("th.select-checkbox").removeClass("unselected");
              // } else {
              //     $("th.select-checkbox").addClass("unselected").removeClass('selected');
              // }

          });

          // Handle click on "Select all" control
          $('#select-all').on('click', function(){
              // // Get all rows with search applied
              // var rows = dataTables.rows({ 'search': 'applied' }).nodes();
              // // Check/uncheck checkboxes for all rows in the table
              // $('input[type="checkbox"]', rows).prop('checked', this.checked);
          });

          // Handle click on checkbox to set state of "Select all" control
          $('#hm-datatable tbody').on('change', 'input[type="checkbox"]', function(){
              // If checkbox is not checked
              if(!this.checked){
                  var el = $('#elect-all').get(0);
                  // If "Select all" control is checked and has 'indeterminate' property
                  if(el && el.checked && ('indeterminate' in el)){
                      // Set visual state of "Select all" control
                      // as 'indeterminate'
                      el.indeterminate = true;
                  }
              }
          });

          $('.emal input').keyup(function () {

              console.log(((this.value.length + 1) * 8) + 4);
          })

      });
  </script>

<?php include 'components/role/addRole.php'; ?>
<?php include 'components/role/editRole.php'; ?>
<?php include 'components/footer.php'; ?>