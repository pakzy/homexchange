<?php include 'components/header.php'; ?>
<?php include 'components/header-menu.php'; ?>
<?php include 'components/sidebar.php'; ?>
<?php include 'components/toast.php'; ?>
  <main class="main-section">
    <div class="container-fluid center-container">
      <div class="row">
        <div class="col-md-8">
          <h1>All Opportunities</h1>
        </div>
        <div class="col-md-4 text-end">
          <a href="javascript:void(0)" data-id="add-role-popup" class="primary-btn-icon open-popup add-opportunity-btn">Create New Opportunity</a>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="data-table-filters">
            <ul>
              <li class="filter-blk">
                <h3>Due in</h3>
                <div class="custom-select" data-id="3">
                  <select>
                    <option>All</option>
                    <option>All</option>
                    <option>Delhi</option>
                    <option>Mumbai</option>
                    <option>Bangalore</option>
                  </select>
                </div>
              </li>
              <li class="filter-blk">
                <h3>Source</h3>
                <div class="custom-select" data-id="5">
                  <select>
                    <option>All</option>
                    <option>All</option>
                    <option>Relationship Manager</option>
                    <option>Home Visit Expert </option>
                  </select>
                </div>
              </li>
              <li class="filter-blk">
                <h3>Status</h3>
                <div class="custom-select" data-id="5">
                  <select>
                    <option>All</option>
                    <option>All</option>
                    <option>Relationship Manager</option>
                    <option>Home Visit Expert </option>
                  </select>
                </div>
              </li>
            </ul>
            <ul>
              <li>
                <div class="dropdown">
                  <a href="javascript:void(0)" class="secondary-btn-icon bulk-action dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    Bulk actions
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="#">Action</a></li>
                    <li><a class="dropdown-item" href="#">Another action</a></li>
                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>


          <table id="hm-datatable-opportunities" class="display" style="width:100%">
            <thead>
            <tr>
              <th><span><input type="checkbox" name="select_all" value="1" id="select-all"></span></th>
              <th>Added on</th>
              <th>Due in</th>
              <th>Customer</th>
              <th>Number</th>
              <th>Source</th>
              <th>Status</th>
              <th>Last Updated</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0; $i < 100; $i++){ ?>
              <tr>
                <td></td>
                <td>2 Feb</td>
                <td class="due-in"><span class="red">3 hours</span></td>
                <td class="customer"><strong>Om Prakash</strong><span>#1234cus_002</span></td>
                <td>9123456789</td>
                <td>Developer</td>
                <td>New</td>
                <td class="last-updated">
                  <h3>Follow up <span>13 Feb</span></h3>
                  <span>Customer did not respond/Busy</span>
                </td>
                <td class="action-td">
                  <div class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                      <img src="assets/img/more_vert.svg">
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                      <li><a class="open-popup" href="javascript:void(0)" data-id="edit-role-popup">Edit</a></li>
                      <li><a class="dropdown-item" href="#">Delete</a></li>
                      <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </main>

  <script>
      $(function () {
          let dataTables = $('#hm-datatable-opportunities').DataTable({
              columnDefs: [ {
                  targets: 0,
                  searchable: false,
                  orderable: false,
                  className: 'select-checkbox',
                  render: function (data, type, full, meta){
                      return '<span><input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '"></span>';
                  }
              },
                  {
                      orderable: false,
                      targets:   8
                  } ],
              language: {
                  searchPlaceholder: "Search",
                  search: "",
                  lengthMenu: "Rows per page _MENU_",
                  paginate: {
                      previous: "",
                      next: ""
                  }
              },
              select: {
                  style:    'os',
                  selector: 'td:first-child span'
              },
              order: [[ 2, 'asc' ]],
              orderCellsTop: true,
              fixedHeader: true,
              initComplete: function () {
                  var api = this.api();
                  // For each column
                  api
                      .columns()
                      .eq(0)
                      .each(function (colIdx) {
                          $(document).click(function (e) {
                              let $this = $(e.target);
                              if($this.hasClass('same-as-selected')){
                                  let val = $this.text();
                                  let data = $this.closest('.custom-select').attr('data-id');
                                  if(val == 'All'){
                                      val = '';
                                  }
                                  if(data == colIdx){
                                      api
                                          .column(colIdx)
                                          .search(val)
                                          .draw();
                                  }
                              }
                          });
                      });

                  let x = $('.bulk-action').offset();
                  $('.dataTables_filter').css({
                      left: (x.left - 490)
                  });
              },
          });
          dataTables.on("click", "th.select-checkbox span", function() {
              var rows = dataTables.rows({ 'search': 'applied' }).nodes();
              let $this = $(this).closest('th');
              if($this.find('input').is(':checked')){
                  $this.removeClass('selected unselected');
                  $this.find('input').prop('checked', false);
                  $('input[type="checkbox"]', rows).prop('checked', false);
                  $('td.select-checkbox').removeClass('selected');
              } else {
                  $this.addClass('selected').removeClass('unselected');
                  $this.find('input').prop('checked', true);
                  $('input[type="checkbox"]', rows).prop('checked', true);
                  $('td.select-checkbox').addClass('selected');
              }

              // if ($("th.select-checkbox").hasClass("selected")) {
              //     dataTables.rows().deselect();
              //     $("th.select-checkbox").removeClass("selected");
              // } else {
              //     dataTables.rows().select();
              //     $("th.select-checkbox").addClass("selected");
              // }
          })
          dataTables.on("click", "td.select-checkbox span ", function(e) {
              let $this = $(this).closest('td');
              if($this.find('input').is(':checked')){
                  $this.removeClass('selected');
                  $this.find('input').prop('checked', false);
              } else {
                  $this.addClass('selected');
                  $this.find('input').prop('checked', true);
              }
              $('td.select-checkbox').each(function () {
                  if(!$(this).find('input').is(':checked')){
                      $('th.select-checkbox').addClass('unselected');
                  }
              })
              // $(this).toggleClass('selected');
              // if (dataTables.rows({
              //     selected: true
              // }).count() !== dataTables.rows().count()) {
              //     $("th.select-checkbox").removeClass("unselected");
              // } else {
              //     $("th.select-checkbox").addClass("unselected").removeClass('selected');
              // }

          });

          // Handle click on "Select all" control
          $('#select-all').on('click', function(){
              // // Get all rows with search applied
              // var rows = dataTables.rows({ 'search': 'applied' }).nodes();
              // // Check/uncheck checkboxes for all rows in the table
              // $('input[type="checkbox"]', rows).prop('checked', this.checked);
          });

          // Handle click on checkbox to set state of "Select all" control
          $('#hm-datatable tbody').on('change', 'input[type="checkbox"]', function(){
              // If checkbox is not checked
              if(!this.checked){
                  var el = $('#elect-all').get(0);
                  // If "Select all" control is checked and has 'indeterminate' property
                  if(el && el.checked && ('indeterminate' in el)){
                      // Set visual state of "Select all" control
                      // as 'indeterminate'
                      el.indeterminate = true;
                  }
              }
          });

      });
  </script>

<?php include 'components/role/addRole.php'; ?>
<?php include 'components/role/editRole.php'; ?>
<?php include 'components/footer.php'; ?>