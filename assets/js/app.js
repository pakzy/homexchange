$(function () {
    setTimeout(function () {
        // $('.dataTables_length label').replaceWith($('<div>' + $('.dataTables_length label').html() + '</div>'));
        $('.dataTables_length label').addClass('custom-select');
        selectOption()
        $('.select-selected').addClass('placeholder-val');
    }, 500);



    $("body").tooltip({ selector: '[data-toggle=tooltip]' });

    setTimeout(function () {
        selectOptionReset('dataTables_length')
        selectOption()
    }, 7000);
    setTimeout(function () {
        // selectOption()
    }, 8000);

    $('.open-popup').click(function () {
        let $class = $(this).attr('data-id');
        $('.'+$class).addClass('active-user-popup');
        setTimeout(function () {
            $('.'+$class).find('.add-user-blk').addClass('active-add-user-blk');
            $('.'+$class).find('.submit-row').addClass('active-add-user-blk');
        }, 500);
    });

    // let fHeight = $('.f-block-col').height();
    // let tHeight = $('.t-block-1').height();
    // let finalHeight = fHeight-(tHeight+30);


    let x = 100 + 28 + 120 + 16 + 50;


    console.log(window.innerHeight);


    function blockHeight() {
        let finalHeight = window.innerHeight - 284;

        $('.l-c-block').css({
            height: (finalHeight + 12)
        });
        $('.f-block-col .f-block-2').css({
            height: finalHeight + 28
        });
        $('.t-block-2-1').css({
            height: finalHeight
        });
    }

    blockHeight();

    $(window).resize(function () {
        blockHeight();
    });



    const el = document.querySelector('div[contenteditable]');

// Get value from element on input events
    el.addEventListener('input', () => {
        if(el.textContent){
            $('div[contenteditable]').addClass('active-edit');
        } else {
            $('div[contenteditable]').removeClass('active-edit');
        }
    });

// Set some value
//     el.textContent = 'Lorem'


    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    });

    $('.create-opportunity-page .side-bar .accordion-button').each(function () {
        $(this).removeClass('collapsed').attr('aria-expanded', false);
        $(this).closest('.accordion-item').find('.accordion-collapse').removeClass('show');
    })

    $('.accordion-header .by-filed').click(function () {
        $(this).closest('.accordion-item').find('.accordion-collapse').slideDown()
    });

    $('.accordion-header .form-field-radio').not('.by-filed').click(function () {
        $(this).closest('.accordion-item').find('.accordion-collapse').slideUp()
    });


    $('.open-add-user-popup').click(function () {
        $('.add-user-popup').addClass('active-user-popup');
        setTimeout(function () {
            $('.add-user-blk, .submit-row').addClass('active-add-user-blk');
        }, 500);
    });



    $('.close-popup').click(function () {
        setTimeout(function () {
            $('.popup-modal').removeClass('active-user-popup');
        }, 500);
        $('.add-user-blk, .submit-row').removeClass('active-add-user-blk');
    });
    
    $('.form-field-radio label').click(function () {
        $(this).closest('.row').find('.form-field-radio label').removeClass('form-field-radio-active');
        $(this).addClass('form-field-radio-active');
    });

    $('.toast-msg').fadeIn(1000);
    setTimeout(function () {
        $('.toast-msg').fadeOut(1000);
    }, 5000);

    $('.checkbox-c label').click(function () {
        $(this).toggleClass('check-active');
    });
    // $('.otp-field input').onkeypress(function () {
    //    let count = $(this).index();
    //    $('.otp-field input').eq(count).trigger('focus');
    // });

    var container = document.getElementsByClassName("otp-field")[0];
    if(container){
        container.onkeyup = function(e) {
            var target = e.srcElement;
            var maxLength = parseInt(target.attributes["maxlength"].value, 2);
            var myLength = target.value.length;
            if (myLength >= maxLength) {
                var next = target;
                while (next = next.nextElementSibling) {
                    if (next == null)
                        break;
                    if (next.tagName.toLowerCase() == "input") {
                        next.focus();
                        break;
                    }
                }
            }
        }
    }


});


function selectOptionReset(selector) {
    /* This function is to reset select box */
    $('.'+selector).find('.select-selected, .select-items, .select-selected').detach();
    // $('.'+selector).find('select')[0].selectedIndex=0;
}

function selectOption() {
    var x, i, j, l, ll, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    l = x.length;
    for (i = 0; i < l; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        ll = selElmnt.length;
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < ll; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h, sl, yl;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this.parentNode.previousSibling;
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        yl = y.length;
                        for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
                $('.placeholder-val').removeClass('placeholder-val');
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function(e) {
            let $this = this;
            if(!$(this).hasClass('select-arrow-active')){
                setTimeout(function () {
                    if(!selectFlag){
                        e.stopPropagation();
                        closeAllSelect($this);
                        $this.nextSibling.classList.toggle("select-hide");
                        $this.classList.toggle("select-arrow-active");
                    }
                }, 100);
            }
        });

    }
    var selectFlag = false;
    $('.select-selected').bind('DOMSubtreeModified', function(){
        selectFlag = true;
        setTimeout(function () {
            selectFlag = false;
        }, 200);
    });
    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, xl, yl, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        xl = x.length;
        yl = y.length;
        for (i = 0; i < yl; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < xl; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);
}