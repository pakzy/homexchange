$(function () {



    let dataTables = $('#hm-datatable').DataTable({
        columnDefs: [ {
            targets: 0,
            searchable: false,
            orderable: false,
            className: 'select-checkbox',
            render: function (data, type, full, meta){
                return '<span><input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '"></span>';
            }
        },
        {
            orderable: false,
            targets:   6
        } ],
        language: {
            searchPlaceholder: "Search",
            search: "",
            lengthMenu: "Rows per page _MENU_",
            paginate: {
                previous: "",
                next: ""
            }
        },
        select: {
            style:    'os',
            selector: 'td:first-child span'
        },
        order: [[ 2, 'asc' ]],
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
            var api = this.api();
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    $(document).click(function (e) {
                        console.log('jjj')
                        let $this = $(e.target);
                        if($this.hasClass('same-as-selected')){
                            let val = $this.text();
                            let data = $this.closest('.custom-select').attr('data-id');
                            if(val == 'All'){
                                val = '';
                            }
                            if(data == colIdx){
                                api
                                    .column(colIdx)
                                    .search(val)
                                    .draw();
                            }
                        }
                    });
                });
        },
        fnDrawCallback: function (oSettings) {
            console.log(this.api().page.info())
        }
    });
    dataTables.on("click", "th.select-checkbox span", function() {
        var rows = dataTables.rows({ 'search': 'applied' }).nodes();
        let $this = $(this).closest('th');
        if($this.find('input').is(':checked')){
            $this.removeClass('selected unselected');
            $this.find('input').prop('checked', false);
            $('input[type="checkbox"]', rows).prop('checked', false);
            $('td.select-checkbox').removeClass('selected');
        } else {
            $this.addClass('selected').removeClass('unselected');
            $this.find('input').prop('checked', true);
            $('input[type="checkbox"]', rows).prop('checked', true);
            $('td.select-checkbox').addClass('selected');
        }

        // if ($("th.select-checkbox").hasClass("selected")) {
        //     dataTables.rows().deselect();
        //     $("th.select-checkbox").removeClass("selected");
        // } else {
        //     dataTables.rows().select();
        //     $("th.select-checkbox").addClass("selected");
        // }
    })
    dataTables.on("click", "td.select-checkbox span ", function(e) {
        let $this = $(this).closest('td');
        if($this.find('input').is(':checked')){
            $this.removeClass('selected');
            $this.find('input').prop('checked', false);
        } else {
            $this.addClass('selected');
            $this.find('input').prop('checked', true);
        }
        $('td.select-checkbox').each(function () {
            if(!$(this).find('input').is(':checked')){
                $('th.select-checkbox').addClass('unselected');
            }
        })
        // $(this).toggleClass('selected');
        // if (dataTables.rows({
        //     selected: true
        // }).count() !== dataTables.rows().count()) {
        //     $("th.select-checkbox").removeClass("unselected");
        // } else {
        //     $("th.select-checkbox").addClass("unselected").removeClass('selected');
        // }

    });


    dataTables.on('click', '.paginate_button', function () {
        debugger
        // var info = table.page.info();
        // $('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
    } );

    // Handle click on "Select all" control
    $('#select-all').on('click', function(){
        // // Get all rows with search applied
        // var rows = dataTables.rows({ 'search': 'applied' }).nodes();
        // // Check/uncheck checkboxes for all rows in the table
        // $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#hm-datatable tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#elect-all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });

});