<main class="create-opportunity-page">
  <?php include 'components/header.php'; ?>
  <?php include 'components/header-menu.php'; ?>
  <?php include 'components/sidebar.php'; ?>
    <section class="main-section">
        <div class="container-fluid center-container">
            <div class="row row-1">
                <div class="col-md-4">

                    <div class="user-detail">
                        <a href="#">
                            <img src="assets/img/left-arrow-red.svg">
                        </a>
                        <div>
                            <h1>Prakash sharma</h1>
                            <a href="tel:9012345678"><img src="assets/img/phone.svg"> 9012345678</a>
                            <a href="mailto:prakash@gmail.com" class="email"><img src="assets/img/email.svg"> <span>Prakash@gmail.com</span></a>
                        </div>
                    </div>

                    <div class="l-c-block">
                        <div class="l-c-detail">
                            <div class="accordion">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="l-d">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-l-d" aria-expanded="true" aria-controls="collapse-l-d">
                                            Lead Details
                                        </button>
                                    </h2>
                                    <div id="collapse-l-d" class="accordion-collapse collapse show" aria-labelledby="heading-l-d">
                                        <div class="accordion-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h6>Lead campaign</h6>
                                                    <p>New build</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6>Lead source </h6>
                                                    <p>Website</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6>Lead sub-source</h6>
                                                    <p>CP</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="l-c-detail">
                            <div class="accordion">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="l-d-1">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-l-d-1" aria-expanded="true" aria-controls="collapse-l-d-1">
                                            Customer details
                                        </button>
                                    </h2>
                                    <div id="collapse-l-d-1" class="accordion-collapse collapse show" aria-labelledby="heading-l-d-1">
                                        <div class="accordion-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h6>Full Name</h6>
                                                    <p>Om Prakash Sharma</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6>Mobile no.</h6>
                                                    <p>+91-1X42X45X74</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6>Alternative no.</h6>
                                                    <p>+91-1X42X45X74</p>
                                                </div>
                                                <div class="col-md-12">
                                                    <h6>Email ID</h6>
                                                    <p>prakash@gmail.com</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6>Sell exisiting home</h6>
                                                    <p>Yes</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6>Purchase new home</h6>
                                                    <p>Flexible</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 f-block-col">
                    <div class="f-block">
                        <div class="f-block-1">
                            <h1><img src="assets/img/calendar_today.svg"> Follow up <strong>call scheduled</strong> for <span class="green">tomorrow</span></h1>
                        </div>
                        <div class="f-block-2" style="height: 600px;">
                            <h2>Data Collection <a href="#" class="edit-btn">Edit</a> </h2>
                            <h3>Lead Details</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <h6>Full name <span>*</span></h6>
                                    <p>Om prakash sharma</p>
                                </div>
                                <div class="col-md-4">
                                    <h6>Mobile No <span>*</span></h6>
                                    <p>+91X2589X796X</p>
                                </div>
                                <div class="col-md-4">
                                    <h6>Alternate contact no.</h6>
                                    <p>+91X2589X796X</p>
                                </div>
                                <div class="col-md-12">
                                    <h6>Email ID</h6>
                                    <p>prakash@gmail.com</p>
                                </div>
                                <div class="col-md-4">
                                    <h6>Lead campaign  <span>*</span></h6>
                                    <p>New build </p>
                                </div>
                                <div class="col-md-4">
                                    <h6>Lead source <span>*</span></h6>
                                    <p>Website</p>
                                </div>
                                <div class="col-md-4">
                                    <h6>Lead sub-source <span>*</span></h6>
                                    <p>CP</p>
                                </div>
                                <div class="col-md-4">
                                    <h6>Sell exisiting property</h6>
                                    <div class="form-field form-field-radio">
                                        <label for="radio-3-1-4" class="form-field-radio-active"><span>Yes</span></label>
                                        <input type="radio" id="radio-3-1-4" name="code4" checked>
                                    </div>
                                    <div class="form-field form-field-radio">
                                        <label for="radio-3-1-5"><span>No</span></label>
                                        <input type="radio" id="radio-3-1-5" name="code4">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h6>Is a govt body alloted property  <span>*</span></h6>
                                    <div class="custom-select">
                                        <select>
                                            <option value="1">Choose an option</option>
                                            <option value="2">Select a role 1</option>
                                            <option value="3">Select a role 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h6>Purchase another home  <span>*</span></h6>
                                    <div class="custom-select">
                                        <select>
                                            <option value="1">Choose an option</option>
                                            <option value="2">Select a role 1</option>
                                            <option value="3">Select a role 2</option>
                                            <option value="3">Select a role 2</option>
                                            <option value="3">Select a role 2</option>
                                            <option value="3">Select a role 2</option>
                                            <option value="3">Select a role 2</option>
                                            <option value="3">Select a role 2</option>
                                            <option value="3">Select a role 2</option>
                                            <option value="3">Select a role 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row submit-row">
                                <div class="col-md-4">
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#schedule-modal"><h4><img src="assets/img/calender.svg"> Schedule Follow-Up</h4></a>
                                </div>
                                <div class="col-md-8">
                                    <a href="#" class="secondary-btn">Save</a>
                                    <a href="#" class="primary-btn disabled-btn">Submit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="t-block">
                        <div class="t-block-1">
                            <p>#1234customer_001</p>
                        </div>
                        <div class="t-block-2">
                            <div class="t-block-2-1">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="timeline-tab" data-bs-toggle="tab" data-bs-target="#timeline" type="button" role="tab" aria-controls="timeline" aria-selected="false">Timeline</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="notes-tab" data-bs-toggle="tab" data-bs-target="#notes" type="button" role="tab" aria-controls="notes" aria-selected="true">Notes</button>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade" id="timeline" role="tabpanel" aria-labelledby="timeline-tab">...</div>
                                    <div class="tab-pane fade show active" id="notes" role="tabpanel" aria-labelledby="notes-tab">
                                        <ul class="n-list">
                                            <li>
                                                <h5>
                                                    Mina
                                                    <a href="#" class="eye" data-bs-toggle="modal" data-bs-target="#confirm-modal"><img src="assets/img/eye.svg"></a>
                                                    <a href="#" class="delete"><img src="assets/img/delete.svg"></a>
                                                </h5>
                                                <h6>2 March, 1:22 PM</h6>
                                                <p>The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog</p>
                                            </li>
                                            <li>
                                                <h5>
                                                    You
                                                </h5>
                                                <h6>2 March, 1:22 PM</h6>
                                                <p>The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog</p>
                                            </li>
                                            <li>
                                                <h5>
                                                    You
                                                </h5>
                                                <h6>2 March, 1:22 PM</h6>
                                                <p>The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog</p>
                                            </li>
                                            <li>
                                                <h5>
                                                    You
                                                </h5>
                                                <h6>2 March, 1:22 PM</h6>
                                                <p>The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row submit-row">
                                <div class="col-md-12">
                                    <div contenteditable></div>
<!--                                    <input type="text" placeholder="Type comment">-->
                                    <a href="#">Cancel</a>
                                    <a href="#">Save</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<!-- Modal -->
<div class="modal fade" id="confirm-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <img src="assets/img/close.svg">
                </button>
                <div class="confirm-blk">
                    <img src="assets/img/visibility.svg">
                    <h2>Are you sure you want to make this note public?</h2>
                    <p>Changes saved cannot be reverted. </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="secondary-btn" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="primary-btn">Yes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="schedule-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Schedule Follow-Up</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <img src="assets/img/close.svg">
                </button>
                <p class="success-msg">Your progress has been auto saved!</p>
            </div>
            <div class="modal-body">
                <div class="m-block">
                    <h6>Select the reason </h6>
                    <div class="form-field form-field-radio">
                        <label for="radio-1"></label>
                        <input type="radio" id="radio-1" name="code"> Customer did not have time
                    </div>
                    <div class="form-field form-field-radio active">
                        <label for="radio-2" class="form-field-radio-active"></label>
                        <input type="radio" id="radio-2" name="code" checked> Customer did not respond/Busy
                    </div>
                    <div class="form-field form-field-radio">
                        <label for="radio-3"></label>
                        <input type="radio" id="radio-3" name="code"> Provided alternate number
                        <div class="form-field">
                            <h6>Alternate contact no. <span><span>*</span></span></h6>
                            <input type="text" placeholder="Type the number">
                        </div>
                    </div>
                    <div class="form-field form-field-radio">
                        <label for="radio-3"></label>
                        <input type="radio" id="radio-3" name="code"> Other
                        <div class="form-field textarea-field">
                            <h6>Please provide reason <span>Max 160 characters</span></h6>
                            <textarea placeholder="Enter the role description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="m-block-2 row">
                    <div class="col-md-6">
                        <h6>Calendar</h6>
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" placeholder="dd/mm/yyyy">
                            <div class="input-group-addon">
                                <img src="assets/img/datepicker.svg">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-field time-form-field">
                            <h6>Time</h6>
                            <input type="text" placeholder="00:00">
                            <div>
                                <a href="#">AM</a>
                                <a href="#" class="active">PM</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="secondary-btn" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="primary-btn">Save</button>
            </div>
        </div>
    </div>
</div>

  <script>
      $(function () {
          let dataTables = $('#hm-datatable-opportunities').DataTable({
              columnDefs: [ {
                  targets: 0,
                  searchable: false,
                  orderable: false,
                  className: 'select-checkbox',
                  render: function (data, type, full, meta){
                      return '<span><input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '"></span>';
                  }
              },
                  {
                      orderable: false,
                      targets:   8
                  } ],
              language: {
                  searchPlaceholder: "Search",
                  search: "",
                  lengthMenu: "Rows per page _MENU_",
                  paginate: {
                      previous: "",
                      next: ""
                  }
              },
              select: {
                  style:    'os',
                  selector: 'td:first-child span'
              },
              order: [[ 2, 'asc' ]],
              orderCellsTop: true,
              fixedHeader: true,
              initComplete: function () {
                  var api = this.api();
                  // For each column
                  api
                      .columns()
                      .eq(0)
                      .each(function (colIdx) {
                          $(document).click(function (e) {
                              let $this = $(e.target);
                              if($this.hasClass('same-as-selected')){
                                  let val = $this.text();
                                  let data = $this.closest('.custom-select').attr('data-id');
                                  if(val == 'All'){
                                      val = '';
                                  }
                                  if(data == colIdx){
                                      api
                                          .column(colIdx)
                                          .search(val)
                                          .draw();
                                  }
                              }
                          });
                      });

                  let x = $('.bulk-action').offset();
                  $('.dataTables_filter').css({
                      left: (x.left - 490)
                  });
              },
          });
          dataTables.on("click", "th.select-checkbox span", function() {
              var rows = dataTables.rows({ 'search': 'applied' }).nodes();
              let $this = $(this).closest('th');
              if($this.find('input').is(':checked')){
                  $this.removeClass('selected unselected');
                  $this.find('input').prop('checked', false);
                  $('input[type="checkbox"]', rows).prop('checked', false);
                  $('td.select-checkbox').removeClass('selected');
              } else {
                  $this.addClass('selected').removeClass('unselected');
                  $this.find('input').prop('checked', true);
                  $('input[type="checkbox"]', rows).prop('checked', true);
                  $('td.select-checkbox').addClass('selected');
              }

              // if ($("th.select-checkbox").hasClass("selected")) {
              //     dataTables.rows().deselect();
              //     $("th.select-checkbox").removeClass("selected");
              // } else {
              //     dataTables.rows().select();
              //     $("th.select-checkbox").addClass("selected");
              // }
          })
          dataTables.on("click", "td.select-checkbox span ", function(e) {
              let $this = $(this).closest('td');
              if($this.find('input').is(':checked')){
                  $this.removeClass('selected');
                  $this.find('input').prop('checked', false);
              } else {
                  $this.addClass('selected');
                  $this.find('input').prop('checked', true);
              }
              $('td.select-checkbox').each(function () {
                  if(!$(this).find('input').is(':checked')){
                      $('th.select-checkbox').addClass('unselected');
                  }
              })
              // $(this).toggleClass('selected');
              // if (dataTables.rows({
              //     selected: true
              // }).count() !== dataTables.rows().count()) {
              //     $("th.select-checkbox").removeClass("unselected");
              // } else {
              //     $("th.select-checkbox").addClass("unselected").removeClass('selected');
              // }

          });

          // Handle click on "Select all" control
          $('#select-all').on('click', function(){
              // // Get all rows with search applied
              // var rows = dataTables.rows({ 'search': 'applied' }).nodes();
              // // Check/uncheck checkboxes for all rows in the table
              // $('input[type="checkbox"]', rows).prop('checked', this.checked);
          });

          // Handle click on checkbox to set state of "Select all" control
          $('#hm-datatable tbody').on('change', 'input[type="checkbox"]', function(){
              // If checkbox is not checked
              if(!this.checked){
                  var el = $('#elect-all').get(0);
                  // If "Select all" control is checked and has 'indeterminate' property
                  if(el && el.checked && ('indeterminate' in el)){
                      // Set visual state of "Select all" control
                      // as 'indeterminate'
                      el.indeterminate = true;
                  }
              }
          });

      });
  </script>

<?php include 'components/role/addRole.php'; ?>
<?php include 'components/role/editRole.php'; ?>
<?php include 'components/footer.php'; ?>