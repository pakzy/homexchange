<main class="relationship-page">
  <?php include 'components/header.php'; ?>
  <?php include 'components/header-menu.php'; ?>
  <?php include 'components/sidebar.php'; ?>
  <section class="main-section pm-section">
    <div class="container-fluid center-container">

        <div  class="normal-table-row bordered-table" style="width: 500px;">
            <div  class="row-table mb-4">
                <div  class="row-head row">
                    <div  class="col"> Project Name</div>
                    <div  class="col"> NH Agreement Value</div>
                    <div  class="col"> OH to NH</div>
                    <div  class="col"> Total Revenue</div>
                    <div  class="col"> Unit Profit</div>
                    <div  class="col"> Unit Profit Margin</div>
                    <div  class="col"> IRR</div></div>
                
                <div  class="row-body row">
                    <div  class="col-md-12">
                        <div  class="row row-blk">
                            <div  class="col"><p >Safal Sky-undefined- </p>
                            </div>
                            <div  class="col"><p >8000000 </p></div>
                            
                            <div  class="col"><p ></p></div>
                            
                            <div  class="col"><p ></p></div>
                            
                            <div  class="col"><p ></p></div>
                            
                            <div  class="col"><p ></p></div>
                            
                            <div  class="col"><p ></p></div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        
        <div class="row header-row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h1>DEALS</h1>
                    </div>
                </div>

                <div class="row row-2">
                    <div class="col-md-4">
                        <img src="assets/img/post_add.svg">
                        <div class="d-blk">
                            <h2>10</h2>
                            <p>New</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="progress-r">
                            <svg id="svg" width="37" height="37" viewport="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <circle id="bar" r="16" cx="18" cy="18" fill="transparent" stroke-dasharray="200" stroke-dashoffset="0" style="stroke-dashoffset: 150px;"></circle>
                            </svg>
                            <img src="assets/img/progress-icon.svg">
                        </div>
                        <div class="d-blk">
                            <h2>12</h2>
                            <p class="yellow">In Progress</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <img src="assets/img/tick.svg">
                        <div class="d-blk">
                            <h2>15</h2>
                            <p class="green">Completed</p>
                        </div>
                    </div>
                </div>
                <div class="form-field range-slider">
                    <div class="range-slider-blk">
                        <input type="range" min="0" max="100"/>
                        <span class="left-val">1000</span>
                        <span class="right-val">1000</span>
                    </div>
                </div>

                <div class="number-field">
                    <button>+</button>
                    <span>12</span>
                    <button>-</button>
                </div>

            </div>
        </div>

        <script>
            $(function () {
                const rangeInputs = document.querySelectorAll('input[type="range"]')

                function handleInputChange(e) {
                    let target = e.target
                    const min = target.min
                    const max = target.max
                    const val = target.value

                    target.style.backgroundSize = (val - min) * 100 / (max - min) + '% 100%'
                }

                rangeInputs.forEach(input => {
                    input.addEventListener('input', handleInputChange)
                })

            })
        </script>

        <div class="row body-row pt-5">
            <div class="col-md-12">
                <div class="row normal-table-row">
                    <div class="col-md-12">
                        <div class="row row-header">
                            <div class="col-md-12">
                                <h4>17 Mar</h4>
                            </div>
                            <div class="col-md-4">
                                <p>Final round offer creation: <span>81,00,000</span></p>
                            </div>
                            <div class="col-md-4">
                                <p>Incentive passed on NH: <span>20%</span></p>
                            </div>
                            <div class="col-md-4">
                                <p>Convenience fees from customer: <span>10%</span></p>
                            </div>
                            <div class="col-md-12 comment-blk">
                                <p>Comments: <span>78L is too less, I was expecting 90L but I am coming down to 85L.</span></p>
                            </div>
                        </div>
                        <div class="row-table">
                            <div class="row-head row">
                                <div class="col">Property</div>
                                <div class="col">NH Agreement Value</div>
                                <div class="col">OH to NH</div>
                                <div class="col">Total Revenue</div>
                                <div class="col">Unit Profit</div>
                                <div class="col">Unit Profit Margin</div>
                                <div class="col">IRR</div>
                            </div>
                            <div class="row-body row">
                                <div class="col">
                                    <h3>Safal Sky</h3>
                                    <p>1 BHK, 1 Crore</p>
                                </div>
                                <div class="col">
                                    <p>200</p>
                                </div>
                                <div class="col">
                                    <p>21%</p>
                                </div>
                                <div class="col">
                                    <p>130</p>
                                </div>
                                <div class="col">
                                    <p>60L</p>
                                </div>
                                <div class="col">
                                    <p>47%</p>
                                </div>
                                <div class="col">
                                    <p>200</p>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>New <a href="#">View all</a> </h1>
                    </div>
                </div>
                <div class="row row-2">
                    <div class="col-md-12">
                        <div class="row row-head">
                            <div class="col">
                                <h4>DUE IN</h4>
                            </div>
                            <div class="col">
                                <h4>LOCATION</h4>
                            </div>
                            <div class="col">
                                <h4>OH SUMMARY</h4>
                            </div>
                            <div class="col">
                                <h4>PH NAME</h4>
                            </div>
                            <div class="col"></div>
                        </div>
                    <?php for ($i = 0; $i < 3; $i++){ ?>
                        <div class="row row-3">
                            <div class="col">
                                <p class="yellow-border">3 Hours</p>
                            </div>
                            <div class="col">
                                <h4>Vasant Society, Bandra</h4>
                                <p class="body-text-5">#1234cus_023</p>
                            </div>
                            <div class="col">
                                <h4>3 BHK</h4>
                                <p class="body-text-5">15000 sq.ft.</p>
                            </div>
                            <div class="col">
                                <h4 class="font-weight-600 dark-grey">Rakesh Sharma</h4>
                            </div>
                            <div class="col"></div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row body-row pt-5">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h1>in progress <a href="#">View all</a></h1>
                    </div>
                </div>
                <div class="row row-2">
                    <div class="col-md-12">
                        <div class="row row-head">
                            <div class="col">
                                <h4>DUE IN</h4>
                            </div>
                            <div class="col">
                                <h4>LOCATION</h4>
                            </div>
                            <div class="col">
                                <h4>OH SUMMARY</h4>
                            </div>
                            <div class="col">
                                <h4>PH NAME</h4>
                            </div>
                            <div class="col">
                                <h4>Last Updated</h4>
                            </div>
                        </div>
                  <?php for ($i = 0; $i < 3; $i++){ ?>
                      <div class="row row-3">
                          <div class="col">
                              <p class="yellow-border">3 Hours</p>
                          </div>
                          <div class="col">
                              <h4>Vasant Society, Bandra</h4>
                              <p class="body-text-5">#1234cus_023</p>
                          </div>
                          <div class="col">
                              <h4>3 BHK <span>Pricing re-evaluation done</span></h4>
                              <p class="body-text-5">15000 sq.ft.</p>
                          </div>
                          <div class="col">
                              <h4 class="font-weight-600 dark-grey">Rakesh Sharma</h4>
                          </div>
                          <div class="col">
                              <p class="body-text-4"><span>Reason:</span> “Please add more
                                  channel partner quotes.”</p>
                          </div>
                      </div>
                  <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row body-row pt-5">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Completed <a href="#">View all</a></h1>
                    </div>
                </div>
                <div class="row row-2">
                    <div class="col-md-12">
                        <div class="row row-head">
                            <div class="col">
                                <h4>COMPLETED ON</h4>
                            </div>
                            <div class="col">
                                <h4>LOCATION</h4>
                            </div>
                            <div class="col">
                                <h4>OH SUMMARY</h4>
                            </div>
                            <div class="col">
                                <h4>PH NAME</h4>
                            </div>
                            <div class="col">
                                <h4>Last Updated</h4>
                            </div>
                        </div>
                      <?php for ($i = 0; $i < 3; $i++){ ?>
                          <div class="row row-3">
                              <div class="col">
                                  <p class="body-text-4 grey font-weight-600">23 April</p>
                              </div>
                              <div class="col">
                                  <h4>Vasant Society, Bandra</h4>
                                  <p class="body-text-5">#1234cus_023</p>
                              </div>
                              <div class="col">
                                  <h4>3 BHK</h4>
                                  <p class="body-text-5">15000 sq.ft.</p>
                              </div>
                              <div class="col">
                                  <h4 class="font-weight-600 dark-grey">Rakesh Sharma</h4>
                              </div>
                              <div class="col">
                                  <p class="body-text-4"><span>Reason:</span> “Please add more
                                      channel partner quotes.”</p>
                              </div>
                          </div>
                      <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
  </section>
</main>
<?php include 'components/role/editRole.php'; ?>
<?php include 'components/footer.php'; ?>