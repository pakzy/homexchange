<?php include 'components/header.php'; ?>
<?php include 'components/header-menu.php'; ?>
<?php include 'components/sidebar.php'; ?>
<?php include 'components/toast.php'; ?>
    <main class="main-section">
        <div class="container-fluid center-container">
            <div class="row">
                <div class="col-md-12">
                    <h1>ALL USERS</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="data-table-filters">
                        <ul>
                            <li class="filter-blk">
                                <h3>Location</h3>
                                <div class="custom-select" data-id="3">
                                    <select>
                                        <option>All</option>
                                        <option>All</option>
                                        <option>Delhi</option>
                                        <option>Mumbai</option>
                                        <option>Bangalore</option>
                                    </select>
                                </div>
                            </li>
                            <li class="filter-blk">
                                <h3>Role</h3>
                                <div class="custom-select" data-id="5">
                                    <select>
                                        <option>All</option>
                                        <option>All</option>
                                        <option>Relationship Manager</option>
                                        <option>Home Visit Expert </option>
                                    </select>
                                </div>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="dropdown">
                                    <a href="javascript:void(0)" class="secondary-btn-icon bulk-action dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        Bulk actions
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Action</a></li>
                                        <li><a class="dropdown-item" href="#">Another action</a></li>
                                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="primary-btn-icon open-add-user-popup">Add New User</a>
                            </li>
                        </ul>
                    </div>


                    <table id="hm-datatable" class="display" style="width:100%">
                        <thead>
                        <tr>
                            <th><span><input type="checkbox" name="select_all" value="1" id="select-all"></span></th>
                            <th>Added on</th>
                            <th>Name</th>
                            <th>Location</th>
                            <th>Number</th>
                            <th>Role</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                            <td>5 Jan</td>
                            <td><div class="name-td"><img src="assets/img/profile_img.png"> Arun Sharma</div></td>
                            <td>Mumbai</td>
                            <td>9123456789</td>
                            <td><strong>Relationship Manager</strong></td>
                            <td class="action-td">
                                <div class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img src="assets/img/more_vert.svg">
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <li><a class="open-popup" href="javascript:void(0)" data-id="edit-user-popup">Edit</a></li>
                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5 Jan</td>
                            <td><div class="name-td"><img src="./assets/img/profile_img.png"> Pankaj Sharma</div></td>
                            <td>Bangalore</td>
                            <td>9123456789</td>
                            <td><strong>Relationship Manager</strong></td>
                            <td class="action-td">
                                <div class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img src="assets/img/more_vert.svg">
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <li><a class="open-popup" href="javascript:void(0)" data-id="edit-user-popup">Edit</a></li>
                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5 Jan</td>
                            <td><div class="name-td"><img src="assets/img/profile_img.png"> Nikhil Singh</div></td>
                            <td>Delhi</td>
                            <td>9123456789</td>
                            <td><strong>Relationship Manager</strong></td>
                            <td class="action-td">
                                <div class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img src="assets/img/more_vert.svg">
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <li><a class="open-popup" href="javascript:void(0)" data-id="edit-user-popup">Edit</a></li>
                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5 Jan</td>
                            <td><div class="name-td"><img src="assets/img/profile_img.png"> Neha Sharma</div></td>
                            <td>Delhi</td>
                            <td>9123456789</td>
                            <td><strong>Home Visit Expert </strong></td>
                            <td class="action-td">
                                <div class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                                        <img src="assets/img/more_vert.svg">
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <li><a class="open-popup" href="javascript:void(0)" data-id="edit-user-popup">Edit</a></li>
                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                            <?php for ($i = 0; $i < 100; $i++){ ?>
                                <tr>
                                    <td></td>
                                    <td>5 Jan</td>
                                    <td><div class="name-td"><img src="assets/img/profile_img.png"> Himanshu Arora <?php echo $i; ?></div></td>
                                    <td>Delhi</td>
                                    <td>9123456789</td>
                                    <td><strong>Relationship Manager</strong></td>
                                    <td class="action-td">
                                        <div class="dropdown">
                                            <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                                                <img src="assets/img/more_vert.svg">
                                            </a>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                                <li><a class="open-popup" href="javascript:void(0)" data-id="edit-user-popup">Edit</a></li>
                                                <li><a class="dropdown-item" href="#">Delete</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>

<?php include 'components/user/addUser.php'; ?>
<?php include 'components/user/editUser.php'; ?>
<?php include 'components/footer.php'; ?>